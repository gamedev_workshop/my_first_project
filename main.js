let hue = 0

function setup() {
  colorMode( HSB, 100, 100, 100 )
  createCanvas( 640, 480 )
  background( 0, 100, 100 )
}

function draw() {
  fill( hue, 100, 100 )
  noStroke()
  circle( mouseX, mouseY, 100 )
  circle( 640 - mouseX, mouseY, 100 )
  circle( mouseX, 480 - mouseY, 100 )
  circle( 640 - mouseX, 480 - mouseY, 100 )
  
  hue = hue + 0.1
  hue = hue % 100
}
